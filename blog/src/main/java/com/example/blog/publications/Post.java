package com.example.blog.publications;

public class Post {
    private Integer id;
    private String title;
    private String text;
    private Integer author;

    public Post(Integer id, String title, String text, Integer author) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.author = author;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getAuthor() {
        return author;
    }

    public void setAuthor(Integer author) {
        this.author = author;
    }
}
