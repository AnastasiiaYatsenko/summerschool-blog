package com.example.blog.publications;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostService {

    private final List<Post> posts;

    public PostService() {
        posts = new ArrayList<>();
        posts.add(new Post(1, "TEST_title_1", "text", 1));
        posts.add(new Post(2, "TEST_title_2", "text", 2));
        posts.add(new Post(3, "TEST_title_3", "text", 3));
        posts.add(new Post(4, "TEST_title_4", "text", 4));
    }

    public List<Post> getAll() {
        return posts;
    }

    public Post getById(int id) {
        for (Post p : posts) {
            if (p.getId().equals(id)) {
                return p;
            }
        }
        return null;
    }
}
