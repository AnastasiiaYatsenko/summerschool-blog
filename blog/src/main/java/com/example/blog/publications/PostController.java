package com.example.blog.publications;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("blog")
public class PostController {

    @Autowired
    RestTemplate restTemplate;

    private final PostAdapter postAdapter;

    public PostController(PostAdapter postAdapter) {
        this.postAdapter = postAdapter;
    }

    @GetMapping("/")
    public String hi() {
        return "Simple get request handled";
    }

    @GetMapping("/all")
    public List<Map<String, Object>> getAll() {
        return postAdapter.getAll();
    }

    @GetMapping("/{id}")
    public Map<String, Object> getById(@PathVariable int id) {
        return postAdapter.getById(id);
    }

    @PutMapping("/{id}")
    public void setById(@PathVariable int id) {
//        update post
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
//        delete post
    }

    @PostMapping("/new")
    public void create() {
//        create post with current user
    }
}