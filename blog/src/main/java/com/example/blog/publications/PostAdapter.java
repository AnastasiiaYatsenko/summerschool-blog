package com.example.blog.publications;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class PostAdapter {

    private static final String REQUESTS_ENDPOINT = "/blog";

    private final String userUrl;
    private final RestTemplate restTemplate;

    private final PostService postService;

    public PostAdapter(@Value("${service.url}") String userUrl,
                          RestTemplateBuilder builder,
                          PostService postService) {
        this.userUrl = userUrl;
        this.restTemplate = builder.build();
        this.postService = postService;
    }

    List<Map<String, Object>> getAll() {
        String path = "/all";

        List<Post> posts =  postService.getAll();
        List<Map<String, Object>> show = new ArrayList<>();
        for (Post p : posts) {
            Map<String, Object> post = new HashMap<String, Object>();
            post.put("id", p.getId());
            post.put("title", p.getTitle());
            post.put("text", p.getText());
            int a = p.getAuthor();
            String url = userUrl + a;
            User user = restTemplate.getForObject(url, User.class);
            assert user != null;
            post.put("author", user.getName());
            show.add(post);
        }
        return show;
    }

    Map<String, Object> getById(int id) {
        Map<String, Object> post = new HashMap<String, Object>();
        Post p = postService.getById(id);
        post.put("id", id);
        post.put("title", p.getTitle());
        post.put("text", p.getText());
        int a = p.getAuthor();
        String url = userUrl + a;
        User user = restTemplate.getForObject(url, User.class);
        assert user != null;
        post.put("author", user.getName());
        return post;
    }
}
