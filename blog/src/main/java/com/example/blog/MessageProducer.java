package com.example.blog;

import com.example.blog.publications.PostService;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
class MessageProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageProducer.class);

    private final String exchange;
    private final String routingKey;
    private final RabbitTemplate rabbitTemplate;

    MessageProducer(@Value("${rabbitmq.topicExchangeName}") String exchange,
                    @Value("${rabbitmq.routingKey}") String routingKey,
                    @Qualifier("blogRabbitTemplate") RabbitTemplate rabbitTemplate) {
        this.exchange = exchange;
        this.routingKey = routingKey;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Scheduled(fixedDelay = 10000)
    void send() {
        System.out.println("Sending message...");
        try {
            PostService postService = new PostService();
            Gson gson = new Gson();
            String postsGson = gson.toJson(postService.getAll());
            rabbitTemplate.convertAndSend(exchange, routingKey, postsGson);
        } catch (AmqpException ex) {
            LOGGER.error("Message '{}' cannot send. {}", "MessageInfo", ex);
        }
    }
}
