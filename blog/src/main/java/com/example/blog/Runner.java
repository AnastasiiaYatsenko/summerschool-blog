package com.example.blog;

import java.util.concurrent.TimeUnit;

import com.example.blog.publications.PostService;
import com.google.gson.Gson;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

//@Component
//public class Runner implements CommandLineRunner {
//
//    private final RabbitTemplate rabbitTemplate;
//    private final Receiver receiver;
//
//    public Runner(Receiver receiver, RabbitTemplate rabbitTemplate) {
//        this.receiver = receiver;
//        this.rabbitTemplate = rabbitTemplate;
//    }
//
//    @Override
//    public void run(String... args) throws Exception {
//        System.out.println("Sending message...");
//        PostService postService = new PostService();
//        Gson gson = new Gson();
//        String postsGson = gson.toJson(postService.getAll());
//        rabbitTemplate.convertAndSend(Application.topicExchangeName, Application.routingKey, postsGson);
//        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
//    }
//
//}