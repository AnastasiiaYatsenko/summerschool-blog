package com.example.comment.comment_structure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("comment")
public class CommentController {

    @Autowired
    RestTemplate restTemplate;

    private final CommentAdapter commentAdapter;

    public CommentController(CommentService commentService, CommentAdapter commentAdapter) {
        this.commentAdapter = commentAdapter;
    }

    @GetMapping("/")
    public String hi() {
        return "Simple get request handled";
    }

    @GetMapping("/all")
    public List<Map<String, Object>> getAll() {

        return commentAdapter.getAll();
    }

    @GetMapping("/{id}")
    public Map<String, Object> getById(@PathVariable int id) {
        return commentAdapter.getById(id);
    }

    @PutMapping("/{id}")
    public void setById(@PathVariable int id) {
//        update comment
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
//        delete comment
    }

    @PostMapping("/new")
    public void create() {
//        create comment with current user
    }
}