package com.example.comment.comment_structure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class CommentAdapter {

    private static final String REQUESTS_ENDPOINT = "/comment";

    private final String userUrl;
    private final RestTemplate restTemplate;

    private final CommentService commentService;

    public CommentAdapter(@Value("${service.url}") String userUrl,
                          RestTemplateBuilder builder,
                          CommentService commentService) {
        this.userUrl = userUrl;
        this.restTemplate = builder.build();
        this.commentService = commentService;
    }

    List<Map<String, Object>> getAll() {
        String path = "/all";

        List<Comment> comments =  commentService.getAll();
        List<Integer> agentsIds = comments.stream().map(Comment::getAuthor).toList();
        List<Map<String, Object>> show = new ArrayList<>();
        for (Comment c : comments) {
            Map<String, Object> comment = new HashMap<String, Object>();
            comment.put("id", c.getId());
            comment.put("text", c.getText());
            int a = c.getAuthor();
            String url = userUrl + a;
            User user = restTemplate.getForObject(url, User.class);
            assert user != null;
            comment.put("author", user.getName());
            comment.put("post", c.getPost());
            show.add(comment);
        }
        return show;
    }

    Map<String, Object> getById(int id) {
        String path = "/{id}";

        Map<String, Object> comment = new HashMap<String, Object>();
        Comment c = commentService.getById(id).get();
        comment.put("id", c.getId());
        comment.put("text", c.getText());
        int a = c.getAuthor();
        String url = userUrl + a;
        User user = restTemplate.getForObject(url, User.class);
        assert user != null;
        comment.put("author", user.getName());
        comment.put("post", c.getPost());
        return comment;
    }
}
