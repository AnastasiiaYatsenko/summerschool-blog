package com.example.comment.comment_structure;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    private List<Comment> comments;

    public CommentService() {
        comments = new ArrayList<>();
        comments.add(new Comment(1, "TEST_text_1", 1, 1));
        comments.add(new Comment(2, "TEST_text_2", 2, 2));
        comments.add(new Comment(3, "TEST_text_3", 3, 3));
        comments.add(new Comment(4, "TEST_text_4", 4, 4));
    }

    public List<Comment> getAll() {
        return comments;
    }

    public Optional<Comment> getById(int id) {
        for (Comment comment : comments) {
            if (comment.getId().equals(id)) {
                return Optional.of(comment);
            }
        }
        return Optional.empty();
    }
}
