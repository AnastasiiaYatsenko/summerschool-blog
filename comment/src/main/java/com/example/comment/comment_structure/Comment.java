package com.example.comment.comment_structure;

public class Comment {
    private Integer id;
    private String text;
    private Integer author;
    private Integer post;

    public Comment(Integer id, String text, Integer author, Integer post) {
        this.id = id;
        this.text = text;
        this.author = author;
        this.post = post;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getAuthor() {
        return author;
    }

    public void setAuthor(Integer author) {
        this.author = author;
    }

    public Integer getPost() {
        return post;
    }

    public void setPost(Integer post) {
        this.post = post;
    }
}
