package com.example.comment;

import com.example.comment.comment_structure.CommentService;
import com.google.gson.Gson;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

//@Component
//public class Runner implements CommandLineRunner {

//    private final RabbitTemplate rabbitTemplate;
//    private final Receiver receiver;
//
//    public Runner(Receiver receiver, RabbitTemplate rabbitTemplate) {
//        this.receiver = receiver;
//        this.rabbitTemplate = rabbitTemplate;
//    }
//
//    @Override
//    public void run(String... args) throws Exception {
//        System.out.println("Sending message...");
//        CommentService commentService = new CommentService();
//        Gson gson = new Gson();
//        String commentsGson = gson.toJson(commentService.getAll());
//        rabbitTemplate.convertAndSend(Application.topicExchangeName, Application.routingKey, commentsGson);
//        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
//    }

//}