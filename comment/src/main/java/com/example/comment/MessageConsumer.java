package com.example.comment;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
class MessageConsumer {

//    private final MessageInfoService messageInfoService;

//    MessageConsumer(MessageInfoService messageInfoService) {
//        this.messageInfoService = messageInfoService;
//    }

    MessageConsumer() {}

    @RabbitListener(queues = "${rabbitmq.get.queue}")
    void messageInfoListener(String message) {
        System.out.println("Received <" + message + ">");
    }
}
