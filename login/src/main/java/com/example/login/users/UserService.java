package com.example.login.users;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private final List<User> users = new ArrayList<>();

    public UserService() {
        users.add(new User(1, "TEST_1"));
        users.add(new User(2, "TEST_2"));
        users.add(new User(3, "TEST_3"));
        users.add(new User(4, "TEST_4"));
    }

    public List<User> getAll() {
        return users;
    }

    public User getById(int id) {
        for (User u : users) {
            if (u.getId().equals(id)) {
                return u;
            }
        }
        return null;
    }
}
